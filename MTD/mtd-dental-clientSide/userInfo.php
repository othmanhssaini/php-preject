<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "mtd-dental";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$last_id = 0;
$email = mysqli_real_escape_string($conn, $_POST["email"]);
$age = mysqli_real_escape_string($conn, $_POST["age"]);
$gender = mysqli_real_escape_string($conn, $_POST["gender"]);
$first_name = mysqli_real_escape_string($conn, $_POST["first_name"]);
$last_name = mysqli_real_escape_string($conn, $_POST["last_name"]);
$address = mysqli_real_escape_string($conn, $_POST["address"]);
$code_postal = mysqli_real_escape_string($conn, $_POST["code_postal"]);
$city = mysqli_real_escape_string($conn, $_POST["city"]);
$country = mysqli_real_escape_string($conn, $_POST["country"]);
$phone = mysqli_real_escape_string($conn, $_POST["phone"]);
$message = mysqli_real_escape_string($conn, $_POST["message"]);
$sql = "INSERT INTO `user` (`email`, `age`, `gender`, `first_name`, `last_name`, `address`, `code_postal`, `city`, `country`, `phone`, `message`) 
VALUES ('$email','$age','$gender','$first_name','$last_name','$address','$code_postal','$city','$country', '$phone', '$message')";
if ($conn->query($sql) === TRUE) {
    $last_id = $conn->insert_id;

} else {
    echo "Error: " . $sql . "<br>" . $conn->error;
}
$array = [];
$result_product = null;
if(isset($_COOKIE['pannier'])){
    $array = json_decode($_COOKIE['pannier'], true);
}
foreach ($array as $key => $value){
    $sql_order="INSERT INTO `orders`(`id_user`, `id_product`, `quantity`) VALUES ($last_id, $key, $value)";
    $conn->query($sql_order);
    setcookie("pannier", "", time() - 3600);
    header("Location: ./finish-shopping.php");
    setcookie("userInfo", true);
}

$conn->close();
?>