<?php 
    if(isset($_COOKIE['userInfo'])){
        $condition = true;
    }else {
        $condition = false;
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>MTD DENTAL</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="assets/img/mtd.jpg">
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/mtd.jpg">

    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/templatemo.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/paiement.css">

    <!-- Load fonts style after rendering the layout styles -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;200;300;400;500;700;900&display=swap">
    <link rel="stylesheet" href="assets/css/fontawesome.min.css">
    <link rel="stylesheet" href="fonts/material-design-iconic-font/css/material-design-iconic-font.css">
	<link rel="stylesheet" href="css/style.css">
    <link href="https://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet">
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light d-none d-lg-block" >
        <div class="container text-light">
            <div class="w-100 d-flex justify-content-between">
                <div>
                    <a class="navbar-brand text-success logo h1 align-self-center" href="index.php">
                        <img src="assets/img/mtd.jpg" class="mtd-logo" alt="Mtd-Logo">
                    </a>
                </div>
                <div class="textColorG ">
                    <i class="fa fa-envelope mx-2"></i>
                    <a class="navbar-sm-brand text-decoration-none" href="mtd-dental@gmx.de">mtd-dental@gmx.de</a>
                    <i class="fa fa-phone mx-2"></i>
                    <a class="navbar-sm-brand textColorG text-decoration-none" href="tel:+49 176 30 66 20 30"> +49 176 30 66 20 30</a>
                </div>
            </div>
        </div>
    </nav>


    <!-- Header -->
    <nav class="navbar navbar-expand-lg navBarColor shadow">
        <div class="container d-flex justify-content-between align-items-center">

            <a class="navbar-brand text-success logo h1 align-self-center" href="index.html"></a>
            <button class="navbar-toggler border-0 navbar-light" type="button" data-bs-toggle="collapse" data-bs-target="#templatemo_main_nav" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="align-self-center collapse navbar-collapse flex-fill  d-lg-flex justify-content-lg-between" id="templatemo_main_nav">
                <div class="flex-fill">
                    <ul class="nav navbar-nav d-flex justify-content-between mx-lg-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="index.php">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="all-products.php">Produktkategorien</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="about-us.php">Über uns</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="contact-us.php">Kontakt</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-bs-toggle="modal" data-bs-target="#templatemo_search">Wonach sichen sie?</a>
                        </li>
                    </ul>
                </div>
                <div class="navbar align-self-center d-flex">
                    <div class="d-lg-none flex-sm-fill mt-3 mb-4 col-7 col-sm-auto pr-3">
                    <form action="./searchByName.php" method="get">
                        <div class="input-group">
                            <input type="text" class="form-control" id="inputMobileSearch" name="q" placeholder="Wonach sichen sie? ...">
                            <button type="submit" class="input-group-text">
                                <i class="fa fa-fw fa-search"></i>
                            </button>
                        </div>
                    </form>
                    </div>
                    <a class="nav-icon d-none d-lg-inline" href="#" data-bs-toggle="modal" data-bs-target="#templatemo_search">
                        <i class="fa fa-fw fa-search text-dark mr-2"></i>
                    </a>
                    <a class="nav-icon position-relative text-decoration-none" href="./pannier.php">
                        <i class="fa fa-fw fa-cart-arrow-down text-dark mr-1"></i>
                        <span class="position-absolute top-0 left-100 translate-middle badge rounded-pill bg-light text-dark"><?php
                         if(isset($_COOKIE['pannier'])){
                            $array = json_decode($_COOKIE['pannier'], true);
                            echo(count($array));
                        }else {
                            echo 0;
                        }
                         ?></span>
                    </a>
                </div>
            </div>

        </div>
    </nav>
    <!-- Close Header -->

    <!-- Modal -->
    <div class="modal fade bg-white" id="templatemo_search" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="w-100 pt-1 mb-5 text-right">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="./searchByName.php" method="get" class="modal-content modal-body border-0 p-0">
                <div class="input-group mb-2">
                    <input type="text" class="form-control" id="inputModalSearch" name="q" placeholder="Wonach sichen sie? ...">
                    <button type="submit" class="input-group-text bg-success text-light">
                        <i class="fa fa-fw fa-search text-white"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>
    <?php if($condition) : ?>
        <section class="bg-light">
            <div class="container py-5">
                <div class="row text-center py-3">
                    <div class="col-lg-6 m-auto">
                        <h1 class="h1">Zahlungsart</h1> <h5>insgesamt zu bezahlen ( <?php 
                        if(isset($_COOKIE['total'])) {
                            echo $_COOKIE['total'];
                          } else {
                            echo 0;
                          }
                        ?> EUR)<h5>
                        <h6>Bitte geben Sie Ihren Vor- und Nachnamen in das Kommentarfeld ein</h6>
                    </div>
                </div>
                <div class="pad50">

                </div>
                <div class="container-inner">
                    <a href="#" target="_blank" class="button paypal" style="pointer-events: none;"><span><i class="fa fa-paypal" aria-hidden="true"></i></span>
                    <p>Paypal Account</p></a><br>
                    <h4>mtd-dental@gmx.de<h4>
                    <br>
                    <a href="https://twitter.com/colorlib" target="_blank" class="button ebay" style="pointer-events: none;"><span><i class="fa fa-university" aria-hidden="true"></i></i></span><p>BANK</p></a><br>
                    <h4>Bank IBAN: DE21 2504 0066 0303 1259 00 / Bank BIC: COBADEFFXXX</h4>
                </div>
            </div>
        </section>
    <?php endif; ?>
    
    <?php if(!$condition) : ?>
        <div class="wrapper">
        <form action="./userInfo.php" id="wizard" method="POST">
            <h2></h2>
            <section>
                <div class="inner">
                    <div class="image-holder">
                        <img src="assets/img/mtd.jpg" alt="">
                    </div>
                    <div class="form-content" >
                        <div class="form-header">
                            <h3>Anmeldung</h3>
                        </div>
                        <p>Bitte geben Sie Ihre Daten ein</p>
                        <div class="form-row">
                            <div class="form-holder">
                                <input name="first_name" type="text" placeholder="Vorname" class="form-control" required>
                            </div>
                            <div class="form-holder">
                                <input name="last_name" type="text" placeholder="Nachname" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-holder">
                                <input name="email" type="text" placeholder="Deine E-Mail" class="form-control" required>
                            </div>
                            <div class="form-holder">
                                <input name="phone" type="text" placeholder="Telefonnummer" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-holder">
                                <input name="age" type="number" min="18" max="130" placeholder="Alter" class="form-control" required>
                            </div>
                            <div class="form-holder" style="align-self: flex-end; transform: translateY(4px);">
                                <div>
                                    <label class="male">
                                        <input type="radio" name="gender" value="male" checked> männlich<br>
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="female">
                                        <input type="radio" name="gender" value="female"> weiblich<br>
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <h2></h2>
            <section>
                <div class="inner">
                    <div class="image-holder">
                        <img src="assets/img/mtd.jpg" alt="">
                    </div>
                    <div class="form-content">
                        <div class="form-header">
                            <h3>Anmeldung</h3>
                        </div>
                        <p>Bitte füllen Sie mit zusätzlichen Informationen</p>
                        <div class="form-row">
                            <div class="form-holder w-100">
                                <input name="address" type="text" placeholder="Adresse" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-holder">
                                <input name="city" type="text" placeholder="Stadt" class="form-control" required>
                            </div>
                            <div class="form-holder">
                                <input name="code_postal" type="text" placeholder="Postleitzahl" class="form-control" required>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-holder">
                                <input name="country" type="text" placeholder="Dein Land" class="form-control" required>
                            </div>
                            <div class="form-holder"></div>
                        </div>
                    </div>
                </div>
            </section>

            <h2></h2>
            <section>
                <div class="inner">
                    <div class="image-holder">
                        <img src="assets/img/mtd.jpg" alt="">
                    </div>
                    <div class="form-content">
                        <div class="form-header">
                            <h3>Anmeldung</h3>
                        </div>
                        <p>Senden Sie eine optionale Nachricht</p>
                        <div class="form-row">
                            <div class="form-holder w-100">
                                <textarea name="message" id="" placeholder="Your messagere here!" class="form-control" style="height: 99px;"></textarea>
                            </div>
                        </div>
                        <div >
                            <label>
                                <input type="checkbox" checked>  Please accept <a href="#">terms and conditions ?</a>
                                <span class="checkmark"></span>
                            </label>
                        </div>
                    </div>
                </div>
            </section>
        </form>
    </div>
    <?php endif; ?>
    

    <!-- Start Footer -->
    <footer class="bg-dark" id="tempaltemo_footer">
        <div class="container">
            <div class="row">

                <div class="col-md-6 pt-5">
                    <h2 class="h2 text-success border-bottom pb-3 border-light logo">MTD DENTAL</h2>
                    <ul class="list-unstyled text-light footer-link-list">
                        <!-- <li>
                            <i class="fas fa-map-marker-alt fa-fw"></i>
                            123 Consectetur at ligula 10660
                        </li> -->
                        <li>
                            <i class="fa fa-phone fa-fw"></i>
                            <a class="text-decoration-none" href="tel:010-020-0340">+49 176 30 66 20 30</a>
                        </li>
                        <li>
                            <i class="fa fa-envelope fa-fw"></i>
                            <a class="text-decoration-none" href="mailto:info@company.com">mtd-dental@gmx.de</a>
                        </li>
                    </ul>
                </div>

                

                <div class="col-md-6 pt-5">
                    <h2 class="h2 text-light border-bottom pb-3 border-light">Die Info</h2>
                    <ul class="list-unstyled text-light footer-link-list">
                        <li class="text-decoration-none"><a href="https://www.ebay.de/str/mtddental" target="_blank">eBay Shop</a></li>
                        <li><a class="text-decoration-none" href="https://www.ebay.de/fdbk/feedback_profile/mtddental?filter=feedback_page:All" target="_blank">Bewertungen</a></li>
                        <li><a class="text-decoration-none" href="https://www.ebay.de/usr/mtddental" target="_blank">Über uns</a></li>
                        <li><a class="text-decoration-none" href="https://contact.ebay.de/ws/eBayISAPI.dll?FindAnswers&frm=284&requested=mtddental" target="_blank">Kontakt</a></li>
                    </ul>
                </div>

            </div>

            <div class="row text-light mb-4">
                <div class="col-12 mb-3">
                    <div class="w-100 my-3 border-top border-light"></div>
                </div>
                <div class="col-auto me-auto">
                    <ul class="list-inline text-left footer-icons">
                        <li class="list-inline-item border border-light rounded-circle text-center">
                            <a class="text-light text-decoration-none" target="_blank" href="http://facebook.com/"><i class="fab fa-facebook-f fa-lg fa-fw"></i></a>
                        </li>
                        <li class="list-inline-item border border-light rounded-circle text-center">
                            <a class="text-light text-decoration-none" target="_blank" href="https://www.instagram.com/"><i class="fab fa-instagram fa-lg fa-fw"></i></a>
                        </li>
                        <li class="list-inline-item border border-light rounded-circle text-center">
                            <a class="text-light text-decoration-none" target="_blank" href="https://www.ebay.de/str/mtddental"><i class="fab fa-ebay fa-lg fa-fw"></i></a>
                        </li>
                        <li class="list-inline-item border border-light rounded-circle text-center">
                            <a class="text-light text-decoration-none" target="_blank" href="https://www.linkedin.com/"><i class="fab fa-linkedin fa-lg fa-fw"></i></a>
                        </li>
                    </ul>
                </div>
                
            </div>
        </div>

        <div class="w-100 bg-black py-3">
            <div class="container">
                <div class="row pt-2">
                    <div class="col-12">
                        <p class="text-left text-light">
                            Copyright &copy; 2021 MTD DENTAL
                            | Designed by <a rel="sponsored" href="https://github.com/benghanemsaad" target="_blank">SB</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>

    </footer>
    <!-- End Footer -->

    <!-- Start Script -->
    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/jquery-migrate-1.2.1.min.js"></script>
    <script src="assets/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/templatemo.js"></script>
    <script src="assets/js/custom.js"></script>
    <script src="js/jquery.steps.js"></script>
    <script src="js/main.js"></script>
    <!-- End Script -->
</body>

</html>