<?php
    function addProjectToPannier($id,$count){
        if(isset($_COOKIE['pannier'])){
            $array = json_decode($_COOKIE['pannier'], true);
        }else {
            $array = [];
        }
        if(is_array($array) && array_key_exists($id, $array)){
            $array[$id] = $count;
            setcookie("pannier", json_encode($array));
        }else {
            $array[$id] = $count;
            setcookie("pannier", json_encode($array));
        }
    }

    function deleteProduct($id){
        if(isset($_COOKIE['pannier'])){
            $array = json_decode($_COOKIE['pannier'], true);
            if(is_array($array) && array_key_exists($id, $array)){
                unset($array[$id]);
                setcookie("pannier", json_encode($array));
            } 
        }
    }

    if(isset($_POST['id_product']) && isset($_POST['quanity'])){
        addProjectToPannier($_POST['id_product'], $_POST['quanity']);
    }

    if(isset($_POST['id_product_To_Delete'])){
        deleteProduct($_POST['id_product_To_Delete']);
    }
?>