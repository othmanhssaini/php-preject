<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "mtd-dental";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$id=0;
if (isset($_GET['id_product'])) {
    $id = $_GET['id_product'];
}

$sql_product = "SELECT * FROM product";
$result_product = $conn->query($sql_product);

$sql_categories =  "SELECT * FROM category";
$result_categories = $conn->query($sql_categories);

$array_of_types=[];
$array_of_status=[];
$array_of_categories=[];

$sql_type = "SELECT * FROM type";
$result_type = $conn->query($sql_type);

if ($result_type->num_rows > 0) {
    // output data of each row
    while($row = $result_type->fetch_assoc()) {
        $array_of_types[$row["id_type"]] = $row["name"];
    }
  } else {
    echo "0 results";
}

$sql_category = "SELECT * FROM category";
$result_category = $conn->query($sql_category);

if ($result_category->num_rows > 0) {
    // output data of each row
    while($row = $result_category->fetch_assoc()) {
        $array_of_categories[$row["id_category"]] = $row["name"];
    }
} else {
    echo "0 results";
}

$sql_status = "SELECT * FROM status";
$result_status = $conn->query($sql_status);

if ($result_status->num_rows > 0) {
    // output data of each row
    while($row = $result_status->fetch_assoc()) {
        $array_of_status[$row["id_status"]] = $row["name"];
    }
} else {
    echo "0 results";
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>MTD DENTAL</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="assets/img/mtd.jpg">
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/mtd.jpg">

    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/templatemo.css">
    <link rel="stylesheet" href="assets/css/custom.css">

    <!-- Load fonts style after rendering the layout styles -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;200;300;400;500;700;900&display=swap">
    <link rel="stylesheet" href="assets/css/fontawesome.min.css">
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light d-none d-lg-block" >
        <div class="container text-light">
            <div class="w-100 d-flex justify-content-between">
                <div>
                    <a class="navbar-brand text-success logo h1 align-self-center" href="index.php">
                        <img src="assets/img/mtd.jpg" class="mtd-logo" alt="Mtd-Logo">
                    </a>
                </div>
                <div class="textColorG ">
                    <i class="fa fa-envelope mx-2"></i>
                    <a class="navbar-sm-brand text-decoration-none" href="mtd-dental@gmx.de">mtd-dental@gmx.de</a>
                    <i class="fa fa-phone mx-2"></i>
                    <a class="navbar-sm-brand textColorG text-decoration-none" href="tel:+49 176 30 66 20 30"> +49 176 30 66 20 30</a>
                </div>
            </div>
        </div>
    </nav>


    <!-- Header -->
    <nav class="navbar navbar-expand-lg navBarColor shadow">
        <div class="container d-flex justify-content-between align-items-center">

            <a class="navbar-brand text-success logo h1 align-self-center" href="index.html"></a>
            <button class="navbar-toggler border-0 navbar-light" type="button" data-bs-toggle="collapse" data-bs-target="#templatemo_main_nav" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="align-self-center collapse navbar-collapse flex-fill  d-lg-flex justify-content-lg-between" id="templatemo_main_nav">
                <div class="flex-fill">
                    <ul class="nav navbar-nav d-flex justify-content-between mx-lg-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="index.php">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="all-products.php">Produktkategorien</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="about-us.php">Über uns</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="contact-us.php">Kontakt</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-bs-toggle="modal" data-bs-target="#templatemo_search">Wonach sichen sie?</a>
                        </li>
                    </ul>
                </div>
                <div class="navbar align-self-center d-flex">
                    <div class="d-lg-none flex-sm-fill mt-3 mb-4 col-7 col-sm-auto pr-3">
                        <form action="./searchByName.php" method="get">
                        <div class="input-group">
                            <input type="text" class="form-control" id="inputMobileSearch" name="q" placeholder="Wonach sichen sie? ...">
                            <button type="submit" class="input-group-text">
                                <i class="fa fa-fw fa-search"></i>
                            </button>
                        </div>
                    </form>
                    </div>
                    <a class="nav-icon d-none d-lg-inline" href="#" data-bs-toggle="modal" data-bs-target="#templatemo_search">
                        <i class="fa fa-fw fa-search text-dark mr-2"></i>
                    </a>
                    <a class="nav-icon position-relative text-decoration-none" href="./pannier.php">
                        <i class="fa fa-fw fa-cart-arrow-down text-dark mr-1"></i>
                        <span class="position-absolute top-0 left-100 translate-middle badge rounded-pill bg-light text-dark"><?php
                         if(isset($_COOKIE['pannier'])){
                            $array = json_decode($_COOKIE['pannier'], true);
                            echo(count($array));
                        }else {
                            echo 0;
                        }
                         ?></span>
                    </a>
                </div>
            </div>

        </div>
    </nav>
    <!-- Close Header -->

    <!-- Modal -->
    <div class="modal fade bg-white" id="templatemo_search" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="w-100 pt-1 mb-5 text-right">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="./searchByName.php" method="get" class="modal-content modal-body border-0 p-0">
                <div class="input-group mb-2">
                    <input type="text" class="form-control" id="inputModalSearch" name="q" placeholder="Search ...">
                    <button type="submit" class="input-group-text bg-success text-light">
                        <i class="fa fa-fw fa-search text-white"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>

    <div class="container py-5">
        <div class="row">
        <h1 class='projTitle'>Alle <span>Kategorien</span>, wenn Sie eine bestimmte möchten, sollten Sie diese auswählen .</h1>
            <div class="col-lg-3">
                <div class="row">
                    <h1 class="title-card">Produktkategorien</h1>
                    <ul class="list-unstyled category-list">
                        <?php 
                            if ($result_categories->num_rows > 0) {
                                // output data of each row
                                while($row = $result_categories->fetch_assoc()) {
                                    echo "<li class='pb-3'>
                                    <a class='collapsed d-flex justify-content-between h3 text-decoration-none' href='./searchByCategory.php?id_category=".$row["id_category"]."'>".$row["name"]."<i class='fa fa-fw fa-chevron-circle-right'></i>
                                    </a>
                                </li>";
                                }
                            }
                        ?>
                    </ul>
                </div>
            </div>
            <div class="col-lg-9">
                <div class="row">
                    <?php 
                        if ($result_product->num_rows > 0) {
                            while($row = $result_product->fetch_assoc()) {
                                if(empty($row["img_url"])){
                                    $row["img_url"] = 'defaultProductIMG.jpeg';
                                }
                                echo "
                                    <div onclick='location.href=\"./single-product.php?id_product=".$row["id_product"]."\";' style='cursor: pointer;' class='col-md-3 customCard'>
                                        <div class='card mb-4 product-wap rounded-0 h-100'>
                                            <div class='card rounded-0'>
                                                <img class='customImg' src='./mtd-dental-admin/uploads/".$row["img_url"]."'>
                                                <div class='card-img-overlay rounded-0 product-overlay d-flex align-items-center justify-content-center'>
                                                    <ul class='list-unstyled'>
                                                        <li><a class='btn btn-success text-white mt-2' href='./single-product.php?id_product=".$row["id_product"]."'><i class='fas fa-cart-plus'></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class='card-body align-items-center d-flex justify-content-center customBody'>
                                                <div >
                                                    <a href='./single-product.php?id_product=".$row["id_product"]."' class='h3 text-decoration-none' style='text-transform: uppercase'>".$row["name"]."</a>
                                                    <p class='text-center mb-0 price-style'>EUR ".$row["price"]."</p>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>";
                            }
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>


    <!-- Start Featured Product -->
    <section class="bg-light">
        <div class="container py-5">
            <div class="row text-center py-3">
                <div class="col-lg-6 m-auto">
                    <h1 class="h1">allgemeine Informationen</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-4 mb-4">
                    <div class="card h-100">
                        <div class="card-body">
                            <h1 class="h1-card">Zahlung</h1>
                            <br><br>
                            <p class="card-text">
                                Sie können zwischen folgenden Zahlungsarten wählen:
                                <br><br>
                                    PayPal Zahlung
                                    Zahlung per Überweisung
                                    Barzahlung bei Abholung
                                    Zahlung per Kreditkarte
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-4 mb-4">
                    <div class="card h-100">

                        <div class="card-body">
                            <h1 class="h1-card">Versand</h1>
                            <br><br>
                            Wir bieten ebenfalls eine persönliche Anlieferung an! Gerne können Sie uns vorab kontaktieren.<br>
                            Ein internationaler Versand per Spedition ist möglich. Bitte kontaktieren Sie uns dazu vorab.
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-4 mb-4">
                    <div class="card h-100">
                        <div class="card-body">
                           
                            <h1 class="h1-card">Impressum</h1>
                            <br><br>
                            <p class="card-text">
                                MTD DENTAL<br>
                                St: DE 323501703
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Featured Product -->

    <!-- Start Footer -->
    <footer class="bg-dark" id="tempaltemo_footer">
        <div class="container">
            <div class="row">

                <div class="col-md-6 pt-5">
                    <h2 class="h2 text-success border-bottom pb-3 border-light logo">MTD DENTAL</h2>
                    <ul class="list-unstyled text-light footer-link-list">
                        <!-- <li>
                            <i class="fas fa-map-marker-alt fa-fw"></i>
                            123 Consectetur at ligula 10660
                        </li> -->
                        <li>
                            <i class="fa fa-phone fa-fw"></i>
                            <a class="text-decoration-none" href="tel:010-020-0340">+49 176 30 66 20 30</a>
                        </li>
                        <li>
                            <i class="fa fa-envelope fa-fw"></i>
                            <a class="text-decoration-none" href="mailto:info@company.com">mtd-dental@gmx.de</a>
                        </li>
                    </ul>
                </div>

                

                <div class="col-md-6 pt-5">
                    <h2 class="h2 text-light border-bottom pb-3 border-light">Die Info</h2>
                    <ul class="list-unstyled text-light footer-link-list">
                        <li class="text-decoration-none"><a href="https://www.ebay.de/str/mtddental" target="_blank">eBay Shop</a></li>
                        <li><a class="text-decoration-none" href="https://www.ebay.de/fdbk/feedback_profile/mtddental?filter=feedback_page:All" target="_blank">Bewertungen</a></li>
                        <li><a class="text-decoration-none" href="https://www.ebay.de/usr/mtddental" target="_blank">Über uns</a></li>
                        <li><a class="text-decoration-none" href="https://contact.ebay.de/ws/eBayISAPI.dll?FindAnswers&frm=284&requested=mtddental" target="_blank">Kontakt</a></li>
                    </ul>
                </div>

            </div>

            <div class="row text-light mb-4">
                <div class="col-12 mb-3">
                    <div class="w-100 my-3 border-top border-light"></div>
                </div>
                <div class="col-auto me-auto">
                    <ul class="list-inline text-left footer-icons">
                        <li class="list-inline-item border border-light rounded-circle text-center">
                            <a class="text-light text-decoration-none" target="_blank" href="http://facebook.com/"><i class="fab fa-facebook-f fa-lg fa-fw"></i></a>
                        </li>
                        <li class="list-inline-item border border-light rounded-circle text-center">
                            <a class="text-light text-decoration-none" target="_blank" href="https://www.instagram.com/"><i class="fab fa-instagram fa-lg fa-fw"></i></a>
                        </li>
                        <li class="list-inline-item border border-light rounded-circle text-center">
                            <a class="text-light text-decoration-none" target="_blank" href="https://www.ebay.de/str/mtddental"><i class="fab fa-ebay fa-lg fa-fw"></i></a>
                        </li>
                        <li class="list-inline-item border border-light rounded-circle text-center">
                            <a class="text-light text-decoration-none" target="_blank" href="https://www.linkedin.com/"><i class="fab fa-linkedin fa-lg fa-fw"></i></a>
                        </li>
                    </ul>
                </div>
                
            </div>
        </div>

        <div class="w-100 bg-black py-3">
            <div class="container">
                <div class="row pt-2">
                    <div class="col-12">
                        <p class="text-left text-light">
                            Copyright &copy; 2021 MTD DENTAL
                            | Designed by <a rel="sponsored" href="https://github.com/benghanemsaad" target="_blank">SB</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>

    </footer>
    <!-- End Footer -->

    <!-- Start Script -->
    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/jquery-migrate-1.2.1.min.js"></script>
    <script src="assets/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/templatemo.js"></script>
    <script src="assets/js/custom.js"></script>
    <!-- End Script -->
</body>

</html>