<?php

if(!(isset($_COOKIE['authenticated']) || $_COOKIE['authenticated'] == true)){
    header('Location: ./login.html');
}

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "mtd-dental";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

//edit vars
$name = '';
$description = '';
$producer = '';
$construction_year = 2000;
$model = '';
$id_category = 0;
$id_status = 0;
$id_type = 0;
$price = 0;
$img_url = 0;
$id=0;
$update = false;


if (isset($_GET['edit'])) {
    $id = $_GET['edit'];
    $update = true;
    $update_query = "SELECT * FROM product WHERE id_product=$id";
    $update_result = $conn->query($update_query);
    if ($update_result->num_rows > 0) {
        while($row = $update_result->fetch_assoc()) {
            $name = $row["name"];
            $description = $row["description"];
            $producer = $row["producer"];
            $construction_year = $row["construction_year"];
            $model = $row["model"];
            $id_category = $row["id_category"];
            $id_status = $row["id_status"];
            $id_type = $row["id_type"];
            $price = $row["price"];
            $img_url = $img_url["img_url"];
        }
    } else {
        echo "0 results";
    }
}

$sql_types = "SELECT * FROM type";
$result_types = $conn->query($sql_types);

$sql_categories = "SELECT * FROM category";
$result_categories = $conn->query($sql_categories);

$sql_status= "SELECT * FROM status";
$result_status = $conn->query($sql_status);
$conn->close();
?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Mtd-dental-Admin</title>
    <meta name="viewport"
        content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
    <meta name="description" content="This is an example dashboard created using build-in elements and components.">
    <meta name="msapplication-tap-highlight" content="no">
    <link href="./main.css" rel="stylesheet">
</head>

<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        <div class="app-header header-shadow">
            <div class="app-header__logo">
                <div class="logo-src"></div>
                <div class="header__pane ml-auto">
                    <div>
                        <button type="button" class="hamburger close-sidebar-btn hamburger--elastic"
                            data-class="closed-sidebar">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="app-header__mobile-menu">
                <div>
                    <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                </div>
            </div>
            <div class="app-header__menu">
                <span>
                    <button type="button"
                        class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                        <span class="btn-icon-wrapper">
                            <i class="fa fa-ellipsis-v fa-w-6"></i>
                        </span>
                    </button>
                </span>
            </div>
            <div class="app-header__content">
                <div class="app-header-right">
                    <div class="header-btn-lg pr-0">
                        <div class="widget-content p-0">
                            <div class="widget-content-wrapper">
                                <div class="widget-content-left">
                                    <div class="btn-group">
                                        <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                                            class="p-0 btn">
                                            <!-- <img width="42" class="rounded-circle" src="assets/images/avatars/1.jpg"
                                                alt=""> -->
                                            <i class="fa fa-angle-down ml-2 opacity-8"></i>
                                        </a>
                                        <div tabindex="-1" role="menu" aria-hidden="true"
                                            class="dropdown-menu dropdown-menu-right">
                                            <button type="button" tabindex="0" onclick="location.href='./logout.php'" class="dropdown-item">Log out</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="widget-content-left  ml-3 header-user-info">
                                    <div class="widget-heading">
                                        Alina Mclourd
                                    </div>
                                    <div class="widget-subheading">
                                        VP People Manager
                                    </div>
                                </div>
                                <div class="widget-content-right header-user-info ml-3">
                                    <button type="button"
                                        class="btn-shadow p-1 btn btn-primary btn-sm show-toastr-example">
                                        <i class="fa text-white fa-calendar pr-1 pl-1"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="ui-theme-settings">
            <button type="button" id="TooltipDemo" class="btn-open-options btn btn-warning">
                <i class="fa fa-cog fa-w-16 fa-spin fa-2x"></i>
            </button>
        </div>
        <div class="app-main">
            <div class="app-sidebar sidebar-shadow">
                <div class="app-header__logo">
                    <div class="logo-src"></div>
                    <div class="header__pane ml-auto">
                        <div>
                            <button type="button" class="hamburger close-sidebar-btn hamburger--elastic"
                                data-class="closed-sidebar">
                                <span class="hamburger-box">
                                    <span class="hamburger-inner"></span>
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="app-header__mobile-menu">
                    <div>
                        <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
                <div class="app-header__menu">
                    <span>
                        <button type="button"
                            class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                            <span class="btn-icon-wrapper">
                                <i class="fa fa-ellipsis-v fa-w-6"></i>
                            </span>
                        </button>
                    </span>
                </div>
                <div class="scrollbar-sidebar">
                    <div class="app-sidebar__inner">
                        <ul class="vertical-nav-menu">
                            
                            <li class="app-sidebar__heading">Article</li>
                            <li>
                                <a href="add-newArticle.php">
                                    <i class="metismenu-icon pe-7s-ticket"></i>
                                    Add new article
                                </a>
                                <a href="all-Articles.php">
                                    <i class="metismenu-icon pe-7s-ticket"></i>
                                    Show all articles
                                </a>
                            </li>
                            <li class="app-sidebar__heading">Types</li>
                            <li>
                                <a href="all-Types.php">
                                    <i class="metismenu-icon pe-7s-ribbon">
                                    </i>Show all types
                                </a>
                            </li>
                            <li>
                                <a href="add-newType.php">
                                    <i class="metismenu-icon pe-7s-ribbon">
                                    </i>Add new type
                                </a>
                            </li>

                            <li class="app-sidebar__heading">Categories</li>
                            <li>
                                <a href="all-Categories.php">
                                    <i class="metismenu-icon pe-7s-box2">
                                    </i>Show all Categories
                                </a>

                                <a href="add-newCategory.php">
                                    <i class="metismenu-icon pe-7s-box2">
                                    </i>Add new Category
                                </a>
                            </li>

                            <li class="app-sidebar__heading">Status</li>
                            <li>
                                <a href="all-Status.php">
                                    <i class="metismenu-icon pe-7s-note2">
                                    </i>Show all Status
                                </a>

                                <a href="add-newStatus.php">
                                    <i class="metismenu-icon pe-7s-note2">
                                    </i>Add new Status
                                </a>
                            </li>
                            <li class="app-sidebar__heading">Orders</li>
                            <li>
                                <a href="orders.php">
                                    <i class="metismenu-icon pe-7s-shopbag">
                                    </i>Show all Orders
                                </a>
                            </li>
                            <li class="app-sidebar__heading">Users</li>
                            <li>
                                <a href="all-users.php">
                                    <i class="metismenu-icon pe-7s-users">
                                    </i>Show all Users
                                </a>
                            </li>
                            
                        </ul>
                    </div>
                </div>
            </div>
            <div class="app-main__outer">
                <div class="app-main__inner">
                    <div class="main-card mb-3 card">
                    <div class="card-body">
                            <h5 class="card-title">Add new Article</h5>
                            <form action="./add-newArticleTraitment.php" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="id" value="<?php echo $id; ?>">    
                            <div class="position-relative form-group">
                                    <label for="titleCategory" class="">Article Name</label>
                                    <input name="name" id="titleCategory" placeholder="Name of article" type="text" class="form-control" value="<?php echo $name; ?>" required>
                                </div>
                                <div class="position-relative form-group">
                                    <label for="descriptionArticle" class="">Description of Article</label>
                                    <textarea name="description" id="descriptionArticle" class="form-control" required><?php echo $description; ?></textarea>
                                </div>
                                <div class="position-relative form-group">
                                    <label for="articleProducer" class="">Producer</label>
                                    <input name="producer" id="articleProducer" placeholder="Peoducer of product" type="text" class="form-control" value="<?php echo $producer; ?>" required>
                                </div>
                                <div class="position-relative form-group">
                                    <label for="price" class="">Price</label>
                                    <input name="price" step="0.01" id="price" placeholder="Price of product" type="number" class="form-control" value="<?php echo $price; ?>" required>
                                </div>
                                <div class="position-relative form-group">
                                    <label for="article_year" class="">Construction year</label>
                                    <input name="construction_year" id="article_year" placeholder="Year of construction" type="number" min="1900" max="2099" step="1" class="form-control" value="<?php echo $construction_year; ?>">
                                </div>
                                <div class="position-relative form-group">
                                    <label for="article_model" class="">Model of product</label>
                                    <input name="model" id="article_model" placeholder="Model of product" type="text" class="form-control" value="<?php echo $model; ?>" required>
                                </div>
                                <div class="position-relative form-group">
                                    <label>Category of product</label>
                                    <select class="mb-2 form-control" name="id_category" id="category" value="<?php echo $id_category; ?>" required>
                                    <option>Select Category</option>
                                    <?php
                                    if ($result_categories->num_rows > 0) {
                                        while($row = $result_categories->fetch_assoc()) {
                                            if($row["id_category"] == $id_category){
                                                echo "<option value=" .$row["id_category"] . " selected='selected'>". $row["name"]. "</option>";
                                            }else {
                                                echo "<option value=" .$row["id_category"] . " >". $row["name"]. "</option>";
                                            }
                                            
                                        }
                                      } else {
                                        echo "0 results";
                                      }
                                      ?>
                                    </select>
                                </div>  
                                <div class="position-relative form-group">
                                    <label>Type of product</label>
                                    <select class="mb-2 form-control" name="id_type" id="type" value="<?php echo $id_type; ?>" required>
                                    <option>Select Type</option>
                                    <?php
                                    if ($result_types->num_rows > 0) {
                                        while($row = $result_types->fetch_assoc()) {
                                            if($row["id_type"] == $id_type){
                                                echo "<option value=" .$row["id_type"] . " selected='selected'>". $row["name"]. "</option>";
                                            }else {
                                                echo "<option value=" .$row["id_type"] . " >". $row["name"]. "</option>";
                                            }
                                        }
                                      } else {
                                        echo "0 results";
                                      }
                                      ?>
                                    </select>
                                </div>  
                                <div class="position-relative form-group">
                                    <label>Status of product</label>
                                    <select class="mb-2 form-control" name="id_status" id="status" value="<?php echo $id_status; ?>" required>
                                    <option>Select Status</option>
                                    <?php
                                    if ($result_status->num_rows > 0) {
                                        while($row = $result_status->fetch_assoc()) {
                                            if($row["id_status"] == $id_status){
                                                echo "<option value=" .$row["id_status"] . " selected='selected'>". $row["name"]. "</option>";
                                            }else {
                                                echo "<option value=" .$row["id_status"] . " >". $row["name"]. "</option>";
                                            }
                                        }
                                      } else {
                                        echo "0 results";
                                      }
                                      ?>
                                    </select>
                                </div>  
                                <div class="position-relative form-group">
                                    <label for="productPicture" class="">Picture of product</label>
                                    <?php if ($update == true): ?>
                                        <input name="imgToUpload" id="productPicture" type="file" class="form-control-file">
                                    <?php else: ?>
                                        <input name="imgToUpload" id="productPicture" type="file" class="form-control-file" required>
                                    <?php endif ?>  
                                </div>
                                <?php if ($update == true): ?>
                                    <button class="mt-1 btn btn-success" type="submit" name="update" >Update</button>
                                <?php else: ?>
                                    <button class="mt-1 btn btn-primary" type="submit" name="save" >Save</button>
                                <?php endif ?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
        </div>
    </div>
    <script type="text/javascript" src="./assets/scripts/main.js"></script>
</body>

</html>