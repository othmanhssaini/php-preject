<?php
define('KB', 1024);
define('MB', 1048576);
define('GB', 1073741824);
define('TB', 1099511627776);

if(!(isset($_COOKIE['authenticated']) || $_COOKIE['authenticated'] == true)){
  header('Location: ./login.html');
}
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "mtd-dental";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
  }
  

$target_dir = "uploads/";
$target_file = $target_dir . basename($_FILES["imgToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
// if(isset($_POST["submit"])) {
  $check = getimagesize($_FILES["imgToUpload"]["tmp_name"]);
  if($check !== false) {
    echo "File is an image - " . $check["mime"] . ".";
    $uploadOk = 1;
  } else {
    echo "File is not an image.";
    $uploadOk = 0;
  }

// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
  }
  
  // Check file size
  if ($_FILES["imgToUpload"]["size"] > 10*MB) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
  }
  
  // Allow certain file formats
  if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
  && $imageFileType != "gif" ) {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
  }
  
  if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
  } else {
    if (move_uploaded_file($_FILES["imgToUpload"]["tmp_name"], $target_file)) {
      echo "The file ". htmlspecialchars( basename( $_FILES["imgToUpload"]["name"])). " has been uploaded.";
    } else {
      echo "Sorry, there was an error uploading your file.";
    }
  }

    $img_url = basename($_FILES["imgToUpload"]["name"]);
    $name = '';
    $description = '';
    $producer = '';
    $construction_year = 2000;
    $model = '';
    $id_category = 0;
    $id_status = 0;
    $id_type = 0;
    $price = 0;
    $id=0;

if(isset($_POST["save"])) {
    $name = mysqli_real_escape_string($conn, $_POST["name"]);
    $description = mysqli_real_escape_string($conn, $_POST["description"]);
    $producer = mysqli_real_escape_string($conn, $_POST["producer"]);
    $construction_year = mysqli_real_escape_string($conn, empty($_POST["construction_year"]) ? 0000 : $_POST["construction_year"]);
    $model = mysqli_real_escape_string($conn, $_POST["model"]);
    $id_category = mysqli_real_escape_string($conn, $_POST["id_category"]);
    $id_status = mysqli_real_escape_string($conn, $_POST["id_status"]);
    $id_type = mysqli_real_escape_string($conn, $_POST["id_type"]);
    $price = mysqli_real_escape_string($conn, $_POST["price"]);
    $sql = "INSERT INTO `product`(`name`, `producer`, `id_type`, `model`, `construction_year`, `id_status`, `description`, `id_category`, `img_url`, `price`) 
    VALUES ('$name','$producer',$id_type,'$model',$construction_year,$id_status,'$description',$id_category,'$img_url', $price)";

    if ($conn->query($sql) === TRUE) {
    header("Location: ./all-Articles.php");
    } else {
    echo "Error: " . $sql . "<br>" . $conn->error;
    }
}else if (isset($_POST["update"])){
    $id = mysqli_real_escape_string($conn, $_POST["id"]);
    $name = mysqli_real_escape_string($conn, $_POST["name"]);
    $description = mysqli_real_escape_string($conn, $_POST["description"]);
    $producer = mysqli_real_escape_string($conn, $_POST["producer"]);
    $construction_year = mysqli_real_escape_string($conn, empty($_POST["construction_year"]) ? 0000 : $_POST["construction_year"]);
    $model = mysqli_real_escape_string($conn, $_POST["model"]);
    $id_category = mysqli_real_escape_string($conn, $_POST["id_category"]);
    $id_status = mysqli_real_escape_string($conn, $_POST["id_status"]);
    $id_type = mysqli_real_escape_string($conn, $_POST["id_type"]);
    $price = mysqli_real_escape_string($conn, $_POST["price"]);
    if(empty($img_url)){
        $sql = "UPDATE `product` SET `name`='$name',`producer`='$producer',`price`=$price,`id_type`=$id_type,`model`='$model',`construction_year`=$construction_year,`id_status`=$id_status,`description`='$description',`id_category`=$id_category WHERE id_product=$id";
    }else {
        $sql = "UPDATE `product` SET `name`='$name',`producer`='$producer',`price`=$price,`id_type`=$id_type,`model`='$model',`construction_year`=$construction_year,`id_status`=$id_status,`description`='$description',`id_category`=$id_category,`img_url`='$img_url' WHERE id_product=$id";
    }
    
    if ($conn->query($sql) === TRUE) {
    header("Location: ./all-Articles.php");
    } else {
    echo "Error: " . $sql . "<br>" . $conn->error;
    }
}


$conn->close();
?>