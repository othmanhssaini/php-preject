
$(function(){
    
    // $('input').filter('[required]:visible').each(function() {
    // if ( $(this).val() === '' )
    //     isValid = false;
    // });
    // if ( $("input:empty").length == 0)
    //     isValid = true;
    // })
    var isValid;
$("input").each(function() {
   var element = $(this);
   if (element.val() == "") {
       isValid = false;
   }
});
	$("#wizard").steps({
        headerTag: "h2",
        bodyTag: "section",
        transitionEffect: "fade",
        enableAllSteps: true,
        transitionEffectSpeed: 500,
        labels: {
            finish: "Einreichen",
            next: "Nach vorne",
            previous: "Rückwärts"
        },
        onFinished: function (event, currentIndex) {
            var isValid = true;
            $("form#wizard :input").each(function() {
                var element = $(this);
                if (element.val() == "") {
                    isValid = false;
                }
             });
            if(isValid){
                $("#wizard").submit();
            }else {
                alert("bitte fülle alle Felder aus");
            }
            
        }
        
    });
    $('.wizard > .steps li a').click(function(){
    	$(this).parent().addClass('checked');
		$(this).parent().prevAll().addClass('checked');
		$(this).parent().nextAll().removeClass('checked');
    });
    // Custome Jquery Step Button
    $('.forward').click(function(){
    	$("#wizard").steps('next');
    })
    $('.backward').click(function(){
        $("#wizard").steps('previous');
    })
    // Select Dropdown
    $('html').click(function() {
        $('.select .dropdown').hide(); 
    });
    $('.select').click(function(event){
        event.stopPropagation();
    });
    $('.select .select-control').click(function(){
        $(this).parent().next().toggle();
    })    
    $('.select .dropdown li').click(function(){
        $(this).parent().toggle();
        var text = $(this).attr('rel');
        $(this).parent().prev().find('div').text(text);
    })
})
